#!/bin/bash

mongo --host localhost --eval 'db = db.getSiblingDB("ceilometer"); db.addUser({user: "ceilometer", pwd: "ceilometer", roles: [ "readWrite", "dbAdmin" ]})'
exit $?